-- MySQL S5 Activity:

-- 1. Return the customerName of the customers who are from the Philippines
select customerName from customers where country = "Philippines"; 


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

select contactLastName, contactFirstName from customers 
where customerName = "La Rochelle Gifts";



-- 3. Return the product name and MSRP of the product named "The Titanic"
select productName, MSRP from products where productName like "%Titanic%";



-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
select firstName, lastName from employees 
where email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of customers who have no registered state
select customerName from customers where state is NULL;


-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
select firstName, lastName, email from employees 
where lastName = "Patterson" AND firstName = "Steve";



-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

select customerName, country, creditLimit from customers 
where country != "USA" AND creditLimit > 3000;


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
select customerNumber from orders where comments like '%DHL%';



-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
select productLine from productlines 
where textDescription like '%state of the art%';



-- 10. Return the countries of customers without duplication

select distinct country from customers;

-- 11. Return the statuses of orders without duplication
select distinct status from orders;


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
select customerName, country from customers 
where country = "USA" or country = "France" or country = "Canada";



-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
select employees.firstName, employees.lastName, offices.city from employees
    join offices on employees.officeCode = offices.officeCode
    where offices.city = "Tokyo";


-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
select customerName from customers
    join employees on customers.salesRepEmployeeNumber = employees.employeeNumber
    where employees.firstName = "Leslie" and employees.lastName= "Thompson";


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
select products.productName, customers.customerName from orderdetails
    join products on orderdetails.productCode = products.productCode
    join orders on orderdetails.orderNumber = orders.orderNumber
    join customers on orders.customerNumber = customers.customerNumber
    where customers.customerName = "Baane Mini Imports";



-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country

select distinct employees.firstName, employees.lastName, customers.customerName, offices.country 
    from payments
    join customers on payments.customerNumber = customers.customerNumber
    join employees on customers.salesRepEmployeeNumber = employees.employeeNumber
    join offices on employees.officeCode = offices.officeCode
    where customers.country = offices.country;



-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

select productName, quantityInStock from products where productLine = "planes" and quantityInStock < 1000;




-- 18. Show the customer's name with a phone number containing "+81".

select customerName from customers where phone like "%+81%";